var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var port = 3000;

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With,   Content-Type, Accept");

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// parse application/json
app.use(bodyParser.json());

// routes
var upload = require('./routes/upload');
var static = require('./routes/static');
var staticID = require('./routes/static-id');
var observation = require('./routes/observation');
var deleteObservation = require('./routes/delete-observation');
var observationID = require('./routes/observation-id');
var csv = require('./routes/csv');
var importCSV = require('./routes/import-csv-database');

app.use('/upload', upload);
app.use('/static', static);
app.use('/staticID', staticID);
app.use('/observation', observation);
app.use('/deleteObservation', deleteObservation);
app.use('/observationID', observationID);
app.use('/csv', csv);
app.use('/importCSV', importCSV);

app.listen(port, function () {
    console.log('listening on port 3000');
});

module.exports = app;
