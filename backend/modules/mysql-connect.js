var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'igadmile'
});

connection.connect();
connection.query('USE baza_suri');

connection.query('CREATE TABLE IF NOT EXISTS opazanja (opazanja_id int(10) NOT NULL AUTO_INCREMENT, datum date, imeGnij varchar(100), tockaOpaz varchar(100), teritorij varchar(200), brojOdraslih int(3), spol varchar(50), starost varchar(50), xGnij float(9,6), yGnij float(9,6), xOpaz float(9,6), yOpaz float(9,6), azimut int(5), napomena varchar(500), PRIMARY KEY(opazanja_id)) DEFAULT CHARSET=utf8');

connection.query('CREATE TABLE IF NOT EXISTS static (static_id int(10) NOT NULL AUTO_INCREMENT, imeGnij varchar(100), tockaOpaz varchar(100), teritorij varchar(200), xGnij float(9,6), yGnij float(9,6), xOpaz float(9,6), yOpaz float(9,6), PRIMARY KEY(static_id)) DEFAULT CHARSET=utf8');

module.exports = connection;
