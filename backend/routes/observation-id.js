var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    var connection = require('../modules/mysql-connect');
    var query = connection.query('SELECT *, DATE_FORMAT(datum,"%Y-%m\-%d\") As datum FROM opazanja WHERE opazanja_id = ?', [req.query.id], function (err, result) {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

module.exports = router;
