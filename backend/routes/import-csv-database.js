var csv = require('fast-csv');
var express = require('express');
var router = express.Router();
var fs = require('fs');

router.get('/', function (req, res) {
    var connection = require('../modules/mysql-connect');
    var table = req.query.table;

    var writeDatabase = function (data) {
        if (table === 'static') {
            connection.query('INSERT INTO static SET ?', data, function (err) {
                if (err) {
                    console.log(err.message);
                } else {
                    console.log('success');
                }
            });
        }
        if (table === 'opazanja') {
            connection.query('INSERT INTO opazanja SET ?', data, function (err) {
                if (err) {
                    console.log(err.message);
                } else {
                    console.log('success');
                }
            });
        }
    };

    var stream = fs.createReadStream('./uploads/' + req.query.csvName);
    csv
        .fromStream(stream, {
            headers: true
        })
        .on('data', function (data) {
            writeDatabase(data);
        })
        .on('end', function () {
            res.send('Unos je završen');
        });
});

module.exports = router;
