var express = require('express');
var router = express.Router();

router.post('/', function (req, res) {
    var connection = require('../modules/mysql-connect');

    connection.query('INSERT INTO opazanja SET ?', req.body,
        function (err) {
            if (err) {
                console.log(err.message);
            } else {
                console.log('success');
            }
        });
    res.end();
});

router.get('/', function (req, res) {
    var connection = require('../modules/mysql-connect');
    connection.query('SELECT * , DATE_FORMAT(datum,"%d\.%m\.%Y") As datum FROM opazanja WHERE datum >= ? AND datum <= ? ORDER BY opazanja_id DESC, DATE(datum) DESC LIMIT ?, ?', [req.query.dateStart, req.query.dateEnd, parseInt(req.query.limit), parseInt(req.query.offset)], function (err, result) {
        if (err) {
            throw err;
        }
        res.send(result);

    });
});

router.put('/', function (req, res) {
    // use bellow when EcmaScript6 is available
    //    var {id, ...post} = req.body:

    var post = {
        datum: req.body.datum,
        imeGnij: req.body.imeGnij,
        tockaOpaz: req.body.tockaOpaz,
        teritorij: req.body.teritorij,
        brojOdraslih: req.body.brojOdraslih,
        spol: req.body.spol,
        starost: req.body.starost,
        xGnij: req.body.xGnij,
        yGnij: req.body.yGnij,
        xOpaz: req.body.xOpaz,
        yOpaz: req.body.yOpaz,
        azimut: req.body.azimut,
        napomena: req.body.napomena
    };
    var connection = require('../modules/mysql-connect');

    connection.query('UPDATE opazanja SET ? WHERE opazanja_id = ?', [post, req.body.opazanja_id], function (err) {
        if (err) {
            console.log(err.message);
        } else {
            console.log('alter success');
        }
    });
    res.end();
});

module.exports = router;
