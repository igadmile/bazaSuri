var csv = require('express-csv');
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    var connection = require('../modules/mysql-connect');
    if (req.query.project == 'svi projekti') {
        var query = connection.query('SELECT * , DATE_FORMAT(datum,"%d\.%m\.%Y") As datum FROM opazanja WHERE datum >= ? AND datum <= ? ORDER BY DATE(datum) ASC', [req.query.dateStart, req.query.dateEnd], function (err, rows, fields) {
            if (err) {
                throw err;
            }
            var headers = {};
            for (key in rows[0]) {
                headers[key] = key;
            }
            rows.unshift(headers);
            res.setHeader('Content-disposition', 'attachment; filename=bazaSuri_' + req.query.project + '.csv');
            res.csv(rows);
        });
    } else {
        var query = connection.query('SELECT * , DATE_FORMAT(datum,"%d\.%m\.%Y") As datum FROM opazanja WHERE projekt in (?) AND datum >= ? AND datum <= ? ORDER BY DATE(datum) ASC', [req.query.project, req.query.dateStart, req.query.dateEnd], function (err, rows, fields) {
            if (err) {
                throw err;
            }
            var headers = {};
            for (key in rows[0]) {
                headers[key] = key;
            }
            rows.unshift(headers);
            res.setHeader('Content-disposition', 'attachment; filename=bazaBIOM_projekt_' + req.query.project + '.csv');
            res.csv(rows);
        });
    }
});

module.exports = router;
