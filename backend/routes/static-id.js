var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  var connection = require('../modules/mysql-connect');
  var query = connection.query('SELECT * FROM static WHERE static_id = ?', [req.query.id],
    function(err, result) {
      if (err) {
        throw err;
      }
      res.send(result);
    });
});

module.exports = router;
