var express = require('express');
var router = express.Router();

router.post('/', function (req, res) {
    var connection = require('../modules/mysql-connect');
    connection.query('DELETE FROM opazanja WHERE opazanja_id = ?', [req.body.opazanja_id], function (err, result) {
        if (err) {
            throw err;
        }
        console.log('observation deleted')
        res.send(result);
    });
});

module.exports = router;
