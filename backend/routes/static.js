var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    var connection = require('../modules/mysql-connect');
    var query = connection.query('SELECT * FROM static', function (err, result) {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

router.post('/', function (req, res) {
    var post = {
        imeGnij: req.body.imeGnij,
        tockaOpaz: req.body.tockaOpaz,
        teritorij: req.body.teritorij,
        xGnij: req.body.xGnij,
        yGnij: req.body.yGnij,
        xOpaz: req.body.xOpaz,
        yOpaz: req.body.yOpaz
    };
    var connection = require('../modules/mysql-connect');

    connection.query('UPDATE static SET ? WHERE static_id = ?', [post, req.body.static_id],
        function (err) {
            if (err) {
                console.log(err.message);
            } else {
                console.log('Update static success');
            }
        });
    res.end();
});

module.exports = router;
