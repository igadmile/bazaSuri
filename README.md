Database for entering and previewing the data for the Golden eagle monitoring.

# Technologies
## Frontend
- angular
- angular-animate
- angular -route
- bootstrap
- leafletjs
- ng-file-upload

## Backend
- nodejs
- express
- mysql

# Feature overview
## Data entry
![data entry](https://gitlab.com/igadmile/bazaSuri/raw/5140cbd5f7437cf01c9bd2ed2b423b7a149e0985/overview_2.jpg)

- Clicking on the markers on the map (which represent predetermined sites for monitoring eagles), shows the data about site right off the map.
- Clicking on the "Dodaj opažanje za ovu točku" opens the data entry form.
- Right of the data entry form is the preview of the last five entries.

## Data preview and edit
![data preview](https://gitlab.com/igadmile/bazaSuri/raw/5140cbd5f7437cf01c9bd2ed2b423b7a149e0985/overview.jpg)

- Data can be filtered for the whole survey period ("Odaberi opažanja za sve datume") or for a desired date range ("Odaberi datume za opažanja").
- When clicked on the button "Prikaži opažanja", the data will appear on the map as well as in the list.
- Clicking on the data entry in the list will open the data summary and enable the user to delete the obesrvaton or edit it.

# Running the app
Before you start, you have to create mysql database named `baza_suri`.
- `git clone`.
- Open the `backend/modules/mysql-connect.js` and edit the mysql user and password.
- Go to `backend/` and run the `npm install` to install dependencies.
- Run the app - `node app.js`.
- Go to `frontend/` and run the `npm install` to install dependencies.
- Run `gulp` to compile `bundle.js`
- Start the web server, eg. `python -m SimpleHTTPServer`
- Open the url in the browser.
- Add the dummy data of monitoring sites.
Click on the "Admin" then choose "Uvoz u baze podataka" and then "Tablica predefiniranih podataka".
Choose the file `backend/uploads/static_import.csv` an press "Submit".
