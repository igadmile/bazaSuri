var route = function ($routeProvider) {
    'use strict';
    $routeProvider
        .when('/entry', {
            templateUrl: 'templates/entry/index.html'
        })
        .when('/view', {
            templateUrl: 'templates/preview/index.html'
        })
        .when('/admin', {
            templateUrl: 'templates/admin/index.html'
        });
};

route.$inject = ['$routeProvider'];
module.exports = route;
