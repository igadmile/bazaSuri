module.exports = function () {
    'use strict';
    return {
        restrict: 'AE',
        templateUrl: 'directives/templates/observation-form-static.html',
        scope: {
            model: "=",
            disabled: "@"
        }
    };
};
