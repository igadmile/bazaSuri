var app = require('angular').module('bazaSuri');

app.directive('addFormInput', require('./form-input'));
app.directive('addFormSelect', require('./form-select'));
app.directive('observationFormStatic', require('./observation-form-static'));
app.directive('observationFormObservation', require('./observation-form-observation'));
app.directive('datepicker', require('./datepicker'));
