module.exports = function () {
    'use strict';
    return {
        restrict: 'AE',
        templateUrl: 'directives/templates/form-input.html',
        scope: {
            label: "@",
            model: "=",
            disabled: "@",
            type: "@",
            idbla: "@"
        }
    };
};
