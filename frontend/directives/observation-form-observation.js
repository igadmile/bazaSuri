module.exports = function () {
    'use strict';
    return {
        restrict: 'AE',
        templateUrl: 'directives/templates/observation-form-observation.html',
        scope: {
            model: "=",
            disabled: "@"
        }
    };
};
