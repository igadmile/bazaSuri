module.exports = function () {
    'use strict';
    return {
        restrict: 'AE',
        templateUrl: 'directives/templates/form-select.html',
        scope: {
            label: "@",
            data: "=",
            model: "=",
            selected: "@",
            disabled: "="
        }
    };
};
