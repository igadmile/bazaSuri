var gulp = require('gulp'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    ngAnnotate = require('gulp-ng-annotate');

gulp.task("default", ["transpile"]);

gulp.task("transpile", function () {
    'use strict';
    return browserify("app.js")
        .bundle()
        .on("error", function (error) {
            console.error("\nError: ", error.message, "\n");
            this.emit("end");
        })
        .pipe(source("bundle.js"))
        .pipe(buffer())
        .pipe(ngAnnotate())
        //        .pipe(uglify())
        .pipe(gulp.dest("dist"));

});



gulp.task("watch", ["transpile"], function () {
    'use strict';
    gulp.watch("./**/*.js", ["transpile"]);
});
