var AddObservaion = function ($scope, uploads, observations) {
    'use strict';
    var L = require('leaflet'),
        map = L.map('map', {
            center: [44.598, 16.589],
            zoom: 7
        }),
        attrib = 'Kartu izradila <a href="http://www.biom.hr/">Udruga BIOM</a>',
        basemap = L.tileLayer('http://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png', {
            attribution: attrib
        }).addTo(map);

    $scope.isDataSelected = false;

    $scope.drawStatic = function () {
        uploads.getStatic()
            .then(function (data) {
                $scope.staticData = data.data;

            })
            .then(function () {
                $scope.staticData.map(function (feature) {
                    var location = new L.LatLng(feature.yGnij, feature.xGnij),
                        marker = new L.Marker(location, {
                            title: feature.imeGnij
                        });
                    marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'><strong>" + feature.imeGnij + "</strong></div>" + "<div style='text-align: center; margin-left: auto; margin-right: auto;'>" + feature.teritorij + "</div>" + "<div style='text-align: center; margin-left: auto; margin-right: auto;'>" + feature.tockaOpaz);
                    marker.on({
                        click: function () {
                            var populateInputs = function (id, value) {
                                var e = document.getElementById(id);
                                e.value = value;
                                var $e = angular.element(e);
                                $e.triggerHandler('input');
                            };
                            Object.keys(feature).map(function (staticValue) {
                                if (staticValue === 'static_id') {} else {
                                    populateInputs(staticValue, feature[staticValue]);
                                }
                            });
                        }
                    });
                    map.addLayer(marker);
                });
            });
    };

    // get last 10 observations
    $scope.getLastObservation = function () {
        var params = {
            limit: 0,
            offset: 10,
            dateStart: '1950-01-01',
            dateEnd: '2200-01-01'
        };
        observations.getObservations(params)
            .then(function (data) {
                $scope.lastObservation = data.data;
            });
    };

    // post observations to serer and reset $scope.allObservations
    $scope.addObservation = function (data) {
        observations.submitObservation(data)
            .then(function () {
                $scope.getLastObservation();
                $scope.isDataSelected = false;
                $scope.enteredData = {};
            });
    };

    $scope.showInput = function () {
        $scope.isDataSelected = true;
    };
};

AddObservaion.$inject = ['$scope', 'uploads', 'observations'];
module.exports = AddObservaion;
