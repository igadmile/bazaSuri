var UploadCSV = function (uploads, $scope) {
    'use strict';
    $scope.uploadCsv = function (file) {
        uploads.uploadFile(file)
            .then(function () {
                uploads.importCsv(file.name, $scope.selectedTable)
                    .then(function (data) {
                        $scope.errorCSV = data;
                        $scope.fileName = null;
                        $scope.selectedTable = null;
                    });
            });
    };
};

UploadCSV.$inject = ['uploads', '$scope'];
module.exports = UploadCSV;
