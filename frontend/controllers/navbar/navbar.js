var NavbarActive = function ($scope, $location) {
    'use strict';
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
};

NavbarActive.$inject = ['$scope', '$location'];
module.exports = NavbarActive;
