var app = require('angular').module('bazaSuri');

app.controller('UpdateObservation', require('./alter-database/update-observation'));
app.controller('UpdateStatic', require('./alter-database/update-static'));
app.controller('AddObservaion', require('./entry/add-observation'));
app.controller('UploadCSV', require('./http/upload-csv'));
app.controller('NavbarActive', require('./navbar/navbar'));
app.controller('GetObservation', require('./preview/get-observation'));
