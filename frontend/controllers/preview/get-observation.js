var GetObservation = function($scope, observations) {
  $scope.allDates = function() {
    $scope.dateSelected = {
      dateStart: '1950-01-01',
      dateEnd: '2200-01-01',
    };
  };

  $scope.selectedDates = function() {
    $scope.checked.all = false;
    $scope.dateSelected = {
      dateStart: null,
      dateEnd: null,
    };
  };
  // TODO not optimal way to get number of records.
  //      should be query that returns length, not all data.
  $scope.numOfObservations = function() {
    var params = {
      limit: 0,
      offset: 1000000000,
      dateStart: $scope.dateSelected.dateStart,
      dateEnd: $scope.dateSelected.dateEnd,
    };
    observations
      .getObservations(params)
      .then(function(data) {
        $scope.totalItems = data.data.length;
        $scope.showResults = true;
      })
      .then(function() {
        $scope.getObservations(1);
      });
  };

  $scope.getObservations = function(data) {
    var params = {
      limit: (data - 1) * 10,
      offset: data * 10,
      dateStart: $scope.dateSelected.dateStart,
      dateEnd: $scope.dateSelected.dateEnd,
    };

    observations.getObservations(params).then(function(data) {
      $scope.showEdit = false;
      $scope.observations = data.data;
      observationGroup.clearLayers();
      data.data.map(function(feature) {
        var location = new L.LatLng(feature.yGnij, feature.xGnij),
          marker = new L.Marker(location, {
            title: feature.imeGnij,
          });
        marker.bindPopup(
          "<div style='text-align: center; margin-left: auto; margin-right: auto;'><strong>" +
            feature.imeGnij +
            '</strong></div>' +
            "<div style='text-align: center; margin-left: auto; margin-right: auto;'>" +
            feature.teritorij +
            '</div>' +
            "<div style='text-align: center; margin-left: auto; margin-right: auto;'>" +
            feature.tockaOpaz
        );
        observationGroup.addLayer(marker);
      });
      map.addLayer(observationGroup);
    });
  };

  $scope.deleteObservation = function(data) {
    observations.deleteObservation(data).then(function() {
      $scope.showEdit = false;
      $scope.numOfObservations();
    });
  };

  $scope.updateObservation = function(data) {
    observations.updateObservation(data).then(function() {
      $scope.showEdit = false;
      $scope.numOfObservations();
    });
  };

  $scope.edit = function(id) {
    observations.getObservationId(id).then(function(data) {
      $scope.editData = data.data[0];
    });
    $scope.showEdit = true;
  };

  $scope.cancelUpdate = function() {
    $scope.showEdit = false;
  };

  var L = require('leaflet'),
    map = L.map('map-preview', {
      center: [44.598, 16.589],
      zoom: 7,
    }),
    attrib = 'Kartu izradila <a href="http://www.biom.hr/">Udruga BIOM</a>',
    basemap = L.tileLayer(
      'http://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png',
      {
        attribution: attrib,
      }
    ).addTo(map),
    observationGroup = new L.layerGroup();

  $scope.showResults = false;
  $scope.currentPage = 1;
};

GetObservation.$inject = ['$scope', 'observations'];
module.exports = GetObservation;
