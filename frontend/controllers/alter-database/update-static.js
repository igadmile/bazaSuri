var UpdateStatic = function ($scope, uploads) {
    'use strict';
    $scope.rowData = {};

    $scope.choosenID = false;

    $scope.getStaticID = function () {
        uploads.getStaticID($scope.rowData.id)
            .then(function (data) {
                if (data.data.length > 0) {
                    $scope.rowData = data.data[0];
                    $scope.rowData.choosenID = true;
                    $scope.rowData.alterWarning = null;
                    $scope.choosenID = true;

                } else {
                    $scope.alterWarning = 'Nema opažanja za uneseni ID!';
                    $scope.choosenID = false;
                }
            });
    };

    $scope.updateStatic = function () {
        uploads.updateStatic($scope.rowData)
            .then(function () {
                $scope.rowData.id = null;
                $scope.rowData.choosenID = false;
            });
    };
};

UpdateStatic.$inject = ['$scope', 'uploads'];
module.exports = UpdateStatic;
