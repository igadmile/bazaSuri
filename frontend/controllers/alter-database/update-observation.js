var UpdateObservation = function ($scope, observations) {
    'use strict';
    // store observations in object
    $scope.enteredData = {};

    $scope.choosenID = false;

    $scope.getDataID = function (id) {
        observations.getObservationId(id)
            .then(function (data) {
                if (data.data.length > 0) {
                    $scope.enteredData = data.data[0];
                    $scope.choosenID = true;
                    $scope.alterWarning = null;
                } else {
                    $scope.alterWarning = 'Nema opažanja za uneseni ID!';
                    $scope.choosenID = false;
                }
            });
    };

    $scope.updateObservation = function () {
        observations.updateObservation($scope.enteredData)
            .then(function () {
                $scope.enteredData.opazanja_id = null;
                $scope.choosenID = false;
            });
    };
    $scope.deleteObservation = function (data) {
        observations.deleteObservation(data)
            .then(function () {
                $scope.enteredData.opazanja_id = null;
                $scope.choosenID = false;
            });
    };
};

UpdateObservation.$inject = ['$scope', 'observations'];
module.exports = UpdateObservation;
