var angular = require('angular');

angular.module('bazaSuri', [require('angular-route'), require('angular-animate'), require('ng-file-upload'), require('angular-ui-bootstrap')]);

angular.module('bazaSuri').config(require('./routes/routes'));
require('./services');
require('./controllers');
require('./directives');
