var uploads = function ($http, Upload) {
    'use strict';
    var url = 'http://localhost:3000/';
    return {
        uploadFile: function (file) {
            file.upload = Upload.upload({
                method: 'POST',
                url: url + 'upload',
                data: {
                    file: file
                }
            });
            return file.upload;
        },
        getStatic: function () {
            return $http({
                method: 'GET',
                url: url + 'static'
            });
        },
        getStaticID: function (id) {
            return $http({
                method: 'GET',
                url: url + 'staticID',
                params: {
                    id: id
                }
            });
        },
        updateStatic: function (data) {
            return $http({
                method: 'POST',
                url: url + 'static',
                data: data
            });
        },
        importCsv: function (name, selectedTable) {
            return $http({
                method: 'GET',
                url: url + 'importCSV',
                params: {
                    csvName: name,
                    table: selectedTable
                }
            });
        }
    };
};

uploads.$inject = ['$http', 'Upload'];
module.exports = uploads;
