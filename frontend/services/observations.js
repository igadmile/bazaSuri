var observations = function ($http) {
    'use strict';
    var url = 'http://localhost:3000/';
    return {
        getObservationId: function (id) {
            return $http.get(url + 'observationID', {
                params: {
                    id: id
                }
            });
        },
        getObservations: function (params) {
            return $http({
                method: 'GET',
                url: url + 'observation',
                params: {
                    limit: params.limit,
                    offset: params.offset,
                    dateStart: params.dateStart,
                    dateEnd: params.dateEnd
                }
            });
        },
        submitObservation: function (data) {
            return $http({
                method: 'POST',
                url: url + 'observation',
                data: data
            });
        },
        updateObservation: function (data) {
            return $http({
                method: 'PUT',
                url: url + 'observation',
                data: data
            });
        },
        deleteObservation: function (data) {
            return $http({
                method: 'POST',
                url: url + 'deleteObservation',
                data: data
            });
        }
    };
};

observations.$inject = ['$http'];
module.exports = observations;
