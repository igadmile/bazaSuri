var app = require('angular').module('bazaSuri');

app.factory('observations', require('./observations'));
app.factory('uploads', require('./uploads'));
